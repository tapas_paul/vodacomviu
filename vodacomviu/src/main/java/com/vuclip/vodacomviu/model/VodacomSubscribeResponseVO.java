package com.vuclip.vodacomviu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.vuclip.vodacomviu.utill.VodacomUtill;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

@Component("vodacomRsaRegistrationResponseVO")
@Scope("prototype")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"payload"
})
//@XmlRootElement(name = "er-response")
@JacksonXmlRootElement(localName = "er-response")
public class VodacomSubscribeResponseVO {

	//LOGGER
	private static final Logger LOGGER = LoggerFactory.getLogger(VodacomSubscribeResponseVO.class.getName());

	//Static JAXB Context
	//private static JAXBContext jaxbContext;

	//@XmlElement(required = true)
	@JacksonXmlProperty(localName = "payload")
	protected Payload payload;

	//@XmlAttribute(name = "sub-id")
	@JacksonXmlProperty(localName = "sub-id",isAttribute=true)
	protected Byte subId;

	//@XmlAttribute(name = "id")
	@JacksonXmlProperty(localName = "id",isAttribute=true)
	protected Integer id;

	//Initializing the JAXB context inside the static block
	/*static {
		try {
			jaxbContext = JAXBContext.newInstance(VodacomRsaRegistrationResponseVO.class);
		} catch (JAXBException ex) {
			LOGGER.error(ex, "VodacomRsaRegistrationResponseVO.static initializer() - Exception occurred ");
		}
	}
	*/
	
	public VodacomSubscribeResponseVO unmarshal(String xmlResponse) {
		VodacomSubscribeResponseVO vodacomResponseVO = null;
		try {
			ObjectMapper xmlMapper = VodacomUtill.getXmlObjectMapper();
			StringReader xmlResponseReader = new StringReader(xmlResponse);
			vodacomResponseVO = xmlMapper.readValue(xmlResponseReader, VodacomSubscribeResponseVO.class);
		} catch(Exception ex) {
			LOGGER.error("VodacomRsaRegistrationResponseVO.unmarshal() - Exception occurred ",ex);
		}
		return vodacomResponseVO;
	}


	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {
			"purchaseOptions",
			"usageAuthorisation"
	})
	public static class Payload {

		//@XmlElement(name = "purchase-options")
		@JacksonXmlProperty(localName = "purchase-options")
		protected PurchaseOptions purchaseOptions;

		//@XmlElement(name = "usage-authorisation")
		@JacksonXmlProperty(localName = "usage-authorisation")
		protected UsageAuthorisation usageAuthorisation;

		public PurchaseOptions getPurchaseOptions() {
			return purchaseOptions;
		}

		public void setPurchaseOptions(Payload.PurchaseOptions value) {
			this.purchaseOptions = value;
		}

		public UsageAuthorisation getUsageAuthorisation() {
			return usageAuthorisation;
		}

		public void setUsageAuthorisation(UsageAuthorisation usageAuthorisation) {
			this.usageAuthorisation = usageAuthorisation;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {
				"subReasonCode",
				"packages"
		})
		public static class PurchaseOptions {

			//@XmlElement(name = "sub-reason-code", required = true)
			@JacksonXmlProperty(localName = "sub-reason-code")
			protected Payload.PurchaseOptions.SubReasonCode subReasonCode;
			//@XmlElement(required = true)
			@JacksonXmlProperty(localName = "packages")
			protected Payload.PurchaseOptions.Packages packages;

			public Payload.PurchaseOptions.SubReasonCode getSubReasonCode() {
				return subReasonCode;
			}

			public void setSubReasonCode(Payload.PurchaseOptions.SubReasonCode value) {
				this.subReasonCode = value;
			}

			public Payload.PurchaseOptions.Packages getPackages() {
				return packages;
			}

			public void setPackages(Payload.PurchaseOptions.Packages value) {
				this.packages = value;
			}


			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {
					"_package"
			})
			public static class Packages {

				//@XmlElement(name = "package", required = true)
				@JacksonXmlProperty(localName = "package")
				protected Payload.PurchaseOptions.Packages.Package _package;

				public Payload.PurchaseOptions.Packages.Package getPackage() {
					return _package;
				}

				public void setPackage(Payload.PurchaseOptions.Packages.Package value) {
					this._package = value;
				}


				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = {
						"id"
				})
				public static class Package {

					//@XmlElement(required = true)
					@JacksonXmlProperty(localName = "id")
					protected String id;

					public String getId() {
						return id;
					}

					public void setId(String value) {
						this.id = value;
					}

				}

			}


			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {
					"code",
					"subCode",
					"name"
			})
			public static class SubReasonCode {

				protected int code;
				//@XmlElement(name = "sub-code")
				@JacksonXmlProperty(localName = "sub-code")
				protected int subCode;
				//@XmlElement(required = true)
				@JacksonXmlProperty(localName = "name")
				protected String name;

				public int getCode() {
					return code;
				}

				public void setCode(int value) {
					this.code = value;
				}

				public int getSubCode() {
					return subCode;
				}

				public void setSubCode(int value) {
					this.subCode = value;
				}

				public String getName() {
					return name;
				}

				public void setName(String value) {
					this.name = value;
				}

			}

		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {
				"isSuccess",
				"servicePackage",
				"packageSubscriptionId",
				"subReasonCode",
				"transactionId"
		})
		public static class UsageAuthorisation {

			//@XmlElement(name = "is-success", required = true)
			@JacksonXmlProperty(localName = "is-success")
			protected String isSuccess;
			//@XmlElement(name = "package")
			@JacksonXmlProperty(localName = "package")
			protected ServicePackage servicePackage;
			//@XmlElement(name = "package-subscription-id")
			@JacksonXmlProperty(localName = "package-subscription-id")
			protected String packageSubscriptionId;
			//@XmlElement(name = "sub-reason-code", required = true)
			@JacksonXmlProperty(localName = "sub-reason-code")
			protected Payload.UsageAuthorisation.SubReasonCode subReasonCode;
			//@XmlElement(name = "transaction-id")
			@JacksonXmlProperty(localName = "transaction-id")
			protected String transactionId;

			public String getIsSuccess() {
				return isSuccess;
			}

			public void setIsSuccess(String value) {
				this.isSuccess = value;
			}

			public ServicePackage getServicePackage() {
				return servicePackage;
			}

			public void setServicePackage(ServicePackage servicePackage) {
				this.servicePackage = servicePackage;
			}

			public String getPackageSubscriptionId() {
				return packageSubscriptionId;
			}

			public void setPackageSubscriptionId(String value) {
				this.packageSubscriptionId = value;
			}

			public Payload.UsageAuthorisation.SubReasonCode getSubReasonCode() {
				return subReasonCode;
			}

			public void setSubReasonCode(Payload.UsageAuthorisation.SubReasonCode value) {
				this.subReasonCode = value;
			}

			public String getTransactionId() {
				return transactionId;
			}

			public void setTransactionId(String value) {
				this.transactionId = value;
			}
			
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {
					"id"
			})
			public static class ServicePackage {
				//@XmlElement(name = "id")
				@JacksonXmlProperty(localName = "id")
				protected String id;

				public String getId() {
					return id;
				}
				public void setId(String id) {
					this.id = id;
				}
			}


			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {
					"code",
					"subCode",
					"name"
			})
			public static class SubReasonCode {
				
				protected String code;
				//@XmlElement(name = "sub-code")
				@JacksonXmlProperty(localName = "sub-code")
				protected String subCode;
				//@XmlElement(required = true)
				@JacksonXmlProperty(localName = "name")
				protected String name;

				public String getCode() {
					return code;
				}

				public void setCode(String value) {
					this.code = value;
				}

				public String getSubCode() {
					return subCode;
				}

				public void setSubCode(String value) {
					this.subCode = value;
				}

				public String getName() {
					return name;
				}

				public void setName(String value) {
					this.name = value;
				}

			}
		}

	}

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	public Byte getSubId() {
		return subId;
	}

	public void setSubId(Byte subId) {
		this.subId = subId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}

