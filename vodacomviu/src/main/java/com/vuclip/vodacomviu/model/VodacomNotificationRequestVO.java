package com.vuclip.vodacomviu.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "payload" })

@JacksonXmlRootElement(localName = "er-response")
public class VodacomNotificationRequestVO {

	@Override
	public String toString() {
		return "VodacomRsaChargingNotificationRequestVO [payload=" + payload + ", externalTransId=" + externalTransId
				+ ", id=" + id + ", msisdn=" + msisdn + "]";
	}

	@JacksonXmlProperty(localName = "payload")
	protected Payload payload;

	@JacksonXmlProperty(localName = "external-trans-id", isAttribute = true)
	protected String externalTransId;

	@JacksonXmlProperty(localName = "id", isAttribute = true)
	protected Integer id;

	@XmlTransient
	private String msisdn;

	//For CC tool logging
		@XmlTransient
		private RequestResponseVO requestResponseVO;
	public RequestResponseVO getRequestResponseVO() {
			return requestResponseVO;
		}


		public void setRequestResponseVO(RequestResponseVO requestResponseVO) {
			this.requestResponseVO = requestResponseVO;
		}

	
	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	public String getExternalTransId() {
		return externalTransId;
	}

	public void setExternalTransId(String subId) {
		this.externalTransId = subId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "usageAuthorisation" })
	public static class Payload {

		@Override
		public String toString() {
			return "Payload [usageAuthorisation=" + usageAuthorisation + "]";
		}

		@XmlElement(name = "usage-authorisation", required = true)
		@JacksonXmlProperty(localName = "usage-authorisation")
		protected UsageAuthorisation usageAuthorisation;

		public UsageAuthorisation getUsageAuthorisation() {
			return usageAuthorisation;
		}

		public void setUsageAuthorisation(UsageAuthorisation value) {
			this.usageAuthorisation = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "isSuccess", "packageSubscriptionId", "reasonCode", "transactionId",
				"errorId", "servicePackage", "paymentStatus" })
		public static class UsageAuthorisation {

			@Override
			public String toString() {
				return "UsageAuthorisation [isSuccess=" + isSuccess + ", packageSubscriptionId=" + packageSubscriptionId
						+ ", reasonCode=" + reasonCode + ", transactionId=" + transactionId + ", errorId=" + errorId
						+ ", servicePackage=" + servicePackage + ", paymentStatus=" + paymentStatus + "]";
			}

			// @XmlElement(name = "is-success", required = true)
			@JacksonXmlProperty(localName = "is-success")
			protected String isSuccess;
			// @XmlElement(name = "package-subscription-id")
			@JacksonXmlProperty(localName = "package-subscription-id")
			protected String packageSubscriptionId;
			// @XmlElement(name = "reason-code", required = true)
			@JacksonXmlProperty(localName = "reason-code")
			protected ReasonCode reasonCode;
			// @XmlElement(name = "transaction-id")
			@JacksonXmlProperty(localName = "transaction-id")
			protected String transactionId;
			// @XmlElement(name = "error-id")
			@JacksonXmlProperty(localName = "error-id")
			protected String errorId;
			// @XmlElement(name = "package")
			@JacksonXmlProperty(localName = "package")
			protected ServicePackage servicePackage;
			// @XmlElement(name = "payment-status")
			@JacksonXmlProperty(localName = "payment-status")
			protected PaymentStatus paymentStatus;

			public ServicePackage getServicePackage() {
				return servicePackage;
			}

			public void setServicePackage(ServicePackage servicePackage) {
				this.servicePackage = servicePackage;
			}

			public String getIsSuccess() {
				return isSuccess;
			}

			public void setIsSuccess(String value) {
				this.isSuccess = value;
			}

			public String getPackageSubscriptionId() {
				return packageSubscriptionId;
			}

			public void setPackageSubscriptionId(String value) {
				this.packageSubscriptionId = value;
			}

			public ReasonCode getReasonCode() {
				return reasonCode;
			}

			public void setReasonCode(ReasonCode value) {
				this.reasonCode = value;
			}

			public String getTransactionId() {
				return transactionId;
			}

			public void setTransactionId(String value) {
				this.transactionId = value;
			}

			public String getErrorId() {
				return errorId;
			}

			public void setErrorId(String errorId) {
				this.errorId = errorId;
			}

			public PaymentStatus getPaymentStatus() {
				return paymentStatus;
			}

			public void setPaymentStatus(PaymentStatus paymentStatus) {
				this.paymentStatus = paymentStatus;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "code", "subCode", "name" })
			public static class ReasonCode {

				@Override
				public String toString() {
					return "ReasonCode [code=" + code + ", subCode=" + subCode + ", name=" + name + "]";
				}

				protected String code;
				// @XmlElement(name = "sub-code")
				@JacksonXmlProperty(localName = "sub-code")
				protected String subCode;
				// @XmlElement(required = true)
				@JacksonXmlProperty(localName = "name")
				protected String name;

				public String getCode() {
					return code;
				}

				public void setCode(String value) {
					this.code = value;
				}

				public String getSubCode() {
					return subCode;
				}

				public void setSubCode(String value) {
					this.subCode = value;
				}

				public String getName() {
					return name;
				}

				public void setName(String value) {
					this.name = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id", "code" })
			public static class PaymentStatus {
				@Override
				public String toString() {
					return "PaymentStatus [id=" + id + ", code=" + code + "]";
				}

				// @XmlElement(name = "id")
				@JacksonXmlProperty(localName = "id")
				protected int id;
				// @XmlElement(name="code",required = true)
				@JacksonXmlProperty(localName = "code")
				protected String code;

				public int getId() {
					return id;
				}

				public void setId(int id) {
					this.id = id;
				}

				public String getCode() {
					return code;
				}

				public void setCode(String code) {
					this.code = code;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "id" })
			public static class ServicePackage {
				@Override
				public String toString() {
					return "ServicePackage [id=" + id + "]";
				}

				// @XmlElement(name = "id")
				@JacksonXmlProperty(localName = "id")
				protected String id;

				public String getId() {
					return id;
				}

				public void setId(String id) {
					this.id = id;
				}
			}

		}
	}
}
