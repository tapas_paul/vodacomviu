/**
 * 
 */
package com.vuclip.vodacomviu.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.vuclip.vodacomviu.model.VodacomSubscribeRequestVO;
import com.vuclip.vodacomviu.model.VodacomSubscribeResponseVO;
import com.vuclip.vodacomviu.model.VodacomSubscribeResponseVO.Payload.PurchaseOptions;

/**
 * @author tpaul
 *
 */
@Controller
public class VodacomSubscribeController {

	private static final int SUBSCRIPTION_NOT_FOUND_REASON_CODE = 41;
	private static final int SUBSCRIPTION_NOT_FOUND_REASON_SUB_CODE = -2;
	private static final String CLIENT_TRNX_ID_PREFIX = "vuc";
	private static final String RSA_ALGORITHM = "RSA";
	private RSAPublicKey rsaPublicKey;

	private static final Logger logger = LoggerFactory.getLogger(VodacomSubscribeController.class);

	@Value("${vodacom.uar.inactivesubcription.url}")
	private String url;

	@Value("${vodacom.redirection.cg.url}")
	private String redirectionCgUrl;

	@Value("${vodacom.partner.redirect.url}")
	private String partnerRedirectUrl;

	Integer channelId;

	@RequestMapping(value = { "/subscribe/pricepoint5" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String subscribeWithPricepoint5(HttpServletRequest request, @RequestParam("msisdn") String msisdn) {
		logger.info("request came to /subscribe");
		try {
			VodacomSubscribeResponseVO vodacomResponseVO = executeRequest(msisdn, channelId);
			if (vodacomResponseVO != null) {
				String packageId = checkUserSubscriptionStatus(vodacomResponseVO);
				if (null != packageId) {
					return redirectToCG(packageId, msisdn);
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	@RequestMapping(value = { "/subscribe/pricepoint20" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String subscribeWithPricepoint20(HttpServletRequest request, @RequestParam("msisdn") String msisdn) {
		logger.info("request came to /subscribe");
		try {
			VodacomSubscribeResponseVO vodacomResponseVO = executeRequest(msisdn, channelId);
			if (vodacomResponseVO != null) {
				String packageId = checkUserSubscriptionStatus(vodacomResponseVO);
				if (null != packageId) {
					return redirectToCG(packageId, msisdn);
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	@RequestMapping(value = { "/subscribe/pricepoint69" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String subscribeWithPricepoint69(HttpServletRequest request, @RequestParam("msisdn") String msisdn) {
		logger.info("request came to /subscribe");
		try {
			VodacomSubscribeResponseVO vodacomResponseVO = executeRequest(msisdn, channelId);
			if (vodacomResponseVO != null) {
				String packageId = checkUserSubscriptionStatus(vodacomResponseVO);
				if (null != packageId) {
					return redirectToCG(packageId, msisdn);
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	@RequestMapping(value = { "/subscribe/pricepoint29" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String subscribeWithPricepoint29(HttpServletRequest request, @RequestParam("msisdn") String msisdn) {
		logger.info("request came to /subscribe");
		channelId = 10;
		try {
			VodacomSubscribeResponseVO vodacomResponseVO = executeRequest(msisdn, channelId);
			if (vodacomResponseVO != null) {
				String packageId = checkUserSubscriptionStatus(vodacomResponseVO);
				if (null != packageId) {
					return redirectToCG(packageId, msisdn);
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	@RequestMapping(value = { "/subscribe/pricepoint39" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String subscribeWithPricepoint39(HttpServletRequest request, @RequestParam("msisdn") String msisdn) {
		logger.info("request came to /subscribe");
		channelId = 20;
		try {
			VodacomSubscribeResponseVO vodacomResponseVO = executeRequest(msisdn, channelId);
			if (vodacomResponseVO != null) {
				String packageId = checkUserSubscriptionStatus(vodacomResponseVO);
				if (null != packageId) {
					return redirectToCG(packageId, msisdn);
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	@RequestMapping(value = { "/subscribe/pricepoint49" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String subscribeWithPricepoint49(HttpServletRequest request, @RequestParam("msisdn") String msisdn) {
		logger.info("request came to /subscribe");
		channelId = 30;
		try {
			VodacomSubscribeResponseVO vodacomResponseVO = executeRequest(msisdn, channelId);
			if (vodacomResponseVO != null) {
				String packageId = checkUserSubscriptionStatus(vodacomResponseVO);
				if (null != packageId) {
					return redirectToCG(packageId, msisdn);
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	@RequestMapping(value = { "/cgRequest" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String mockCgRequest(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request came to /cgRequest");

		String callbackUrl = request.getParameter("partner-redirect-url") + "?client-txn-id="
				+ request.getParameter("client-txn-id")
				+ "&status-code=0&result=ACCEPTED&result-description=Successfully subscribed";
		logger.info("callbackUrl is: " + callbackUrl);
		return "redirect:" + callbackUrl;
	}

	@RequestMapping(value = { "/cgcallback" }, method = { RequestMethod.POST, RequestMethod.GET })
	public String cgCallBack(HttpServletRequest request, Model model) {
		logger.info("request came to /cgcallback");
		String queryString = request.getQueryString();

		logger.info("Redirection in cgcallback:" + request.getRequestURL() + "?" + queryString);
		String CgCallbackResponse = request.getRequestURL() + "?" + queryString;
		model.addAttribute("statusCode", request.getParameter("status-code"));
		model.addAttribute("clientTxnId", request.getParameter("client-txn-id"));
		model.addAttribute("cgResponse", CgCallbackResponse);

		return "cgcallbackresponse";
	}

	private VodacomSubscribeResponseVO executeRequest(String msisdn, Integer channelId2) {
		try {
			RestTemplate restTemplate = new RestTemplate(
					new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Basic RENCX1ZVQ0xJUDp2dWNsaXBAdm9kYWNvbTEy");
			headers.add("Accept", "application/xml");
			HttpEntity<String> req = new HttpEntity<String>(
					new VodacomSubscribeRequestVO().populateRequestData(msisdn, channelId2).marshal(), headers);
			ResponseEntity<String> resp = restTemplate.exchange(url, HttpMethod.POST, req, String.class);
			String subscriptionResponse = resp.getBody();
			logger.info("Response received from carrier API: " + subscriptionResponse);
			VodacomSubscribeResponseVO vodacomResponseVO = new VodacomSubscribeResponseVO()
					.unmarshal(subscriptionResponse);
			return vodacomResponseVO;
		} catch (Exception e) {
			logger.error("Exception occurred ", e);
		}
		return null;
	}

	private String checkUserSubscriptionStatus(VodacomSubscribeResponseVO vodacomResponseVO) {
		Integer reasonCode = null;
		Integer reasonSubCode = null;
		String packageId = null;
		PurchaseOptions purchaseOptions = vodacomResponseVO.getPayload().getPurchaseOptions();
		if (purchaseOptions != null) {
			reasonCode = purchaseOptions.getSubReasonCode().getCode();
			reasonSubCode = purchaseOptions.getSubReasonCode().getSubCode();
			// Check if User subscription is active or not
			if (isSubscriptionNotFound(reasonCode, reasonSubCode)) {
				// Get the package-id
				packageId = vodacomResponseVO.getPayload().getPurchaseOptions().getPackages().getPackage().getId();
			}
		} else {
			if (vodacomResponseVO.getPayload().getUsageAuthorisation() != null
					&& Boolean.valueOf(vodacomResponseVO.getPayload().getUsageAuthorisation().getIsSuccess())) {
				logger.info("User Alreday Subscribed at Vodacom end");
			}
		}
		return packageId;
	}

	private String redirectToCG(String packageId, String msisdn) {
		StringBuilder redirectUrl = new StringBuilder();
		redirectUrl.append(redirectionCgUrl);
		redirectUrl.append("?client-txn-id=");
		redirectUrl.append(CLIENT_TRNX_ID_PREFIX.concat(String.valueOf(generateUniqueClientTxnId())));
		redirectUrl.append("&token=");
		redirectUrl.append(encryptMsisdn(msisdn.trim()));
		redirectUrl.append("&partner-id=DCB_VIU");
		redirectUrl.append("&package-id=");
		redirectUrl.append(packageId);
		redirectUrl.append("&partner-redirect-url=");
		redirectUrl.append(partnerRedirectUrl);

		return "redirect:" + redirectUrl.toString();
	}

	private boolean isSubscriptionNotFound(Integer reasonCode, Integer reasonSubCode) {
		return reasonCode == SUBSCRIPTION_NOT_FOUND_REASON_CODE
				&& reasonSubCode == SUBSCRIPTION_NOT_FOUND_REASON_SUB_CODE;
	}

	private int generateUniqueClientTxnId() {
		return new Random().nextInt(999999 - 100000 + 1) + 100000;
	}

	private String encryptMsisdn(String msisdn) {

		String rsaEncryptedMsisdn = null;
		try {
			rsaEncryptedMsisdn = getRSAencryptedMsisdn(msisdn);
		} catch (IOException | GeneralSecurityException e) {
			logger.error("Error occured while encrypting the msisdn : {} ", e);
		}
		logger.info("RSA encryption done, actual msisdn: {}, encrypted msisdn: {}", msisdn, rsaEncryptedMsisdn);
		return rsaEncryptedMsisdn;
	}

	@PostConstruct
	private void init() throws IOException, GeneralSecurityException {

		String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs0iOjcXl/+Tz9RHv373JCD1RgfH/VPJnWPpy0W5Hmfo4kE8/cI8Hu2ALJ37vc2TD7X28+/kZ0cCKxvcOfEJ6NTEq0b2zPylSUc7zuliYbw66G5mulUo3tXLP0Rht1i6OiZwJb9nAvkc/bzuwN0IkbSu7CwY1QHhAVu4F0lL7gCmxGnLR42hAbNacTa1CAea01646MaHH4aJB7c7jKnBmZCZHgZSlHjW3CTlBUtbuGJfma9Qjaii/rh1IQBPw4lw+31pFKOAcai2Fq8NimfJruqP6dv5vY0e405mb/GABG1+3JbeLbnyE2XwCJcsKhiyJ3bvHAnysEC49B1cFYj5VewIDAQAB";
		byte[] encodedKey = Base64.decodeBase64(publicKey);
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedKey);
		KeyFactory kf = KeyFactory.getInstance(RSA_ALGORITHM);
		rsaPublicKey = (RSAPublicKey) kf.generatePublic(publicKeySpec);
	}

	public String getRSAencryptedMsisdn(String msisdn) throws IOException, GeneralSecurityException {
		PublicKey publicKey = rsaPublicKey;
		Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] decryptedBytes = cipher.doFinal(msisdn.getBytes());
		return DatatypeConverter.printHexBinary(decryptedBytes);
	}
}