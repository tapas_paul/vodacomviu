package com.vuclip.vodacomviu.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.vuclip.vodacomviu.model.VodacomUnsubscribeRequestVO.Payload.InactivateSubscription;
import com.vuclip.vodacomviu.utill.VodacomUtill;

@Component("vodacomRsaCancelRequestVO")
@Scope("prototype")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "payload" })
// @XmlRootElement(name = "er-request")
@JacksonXmlRootElement(localName = "er-request")
public class VodacomUnsubscribeRequestVO {

	// LOGGER
	private static final Logger LOGGER = LoggerFactory.getLogger(VodacomUnsubscribeRequestVO.class.getName());

	// Static JAXB Context
	// private static JAXBContext jaxbContext;

	// @XmlElement(required = true)
	@JacksonXmlProperty(localName = "payload")
	protected Payload payload;

	// @XmlAttribute(name = "purchase_locale")
	@JacksonXmlProperty(localName = "purchase_locale", isAttribute = true)
	protected String purchaseLocale;

	// @XmlAttribute(name = "id")
	@JacksonXmlProperty(localName = "id", isAttribute = true)
	protected Integer id;

	// @XmlAttribute(name = "client-application-id")
	@JacksonXmlProperty(localName = "client-application-id", isAttribute = true)
	protected String clientApplicationId;

	// @XmlAttribute(name = "language_locale")
	@JacksonXmlProperty(localName = "language_locale", isAttribute = true)
	protected String languageLocale;

	@XmlTransient
	protected String subscriptionStatusId;

	public String getSubscriptionStatusId() {
		return subscriptionStatusId;
	}

	public void setSubscriptionStatusId(String subscriptionStatusId) {
		this.subscriptionStatusId = subscriptionStatusId;
	}

	public VodacomUnsubscribeRequestVO() {
		this.payload = new Payload();
	}

	public VodacomUnsubscribeRequestVO populateRequestData(String msisdn, String subscriptionStatusId) {
		// Purchase Locale
		VodacomUnsubscribeRequestVO vodacomRsaCancelRequestVO = new VodacomUnsubscribeRequestVO();
		Payload payload = new Payload();
		InactivateSubscription inactivateSubscription = new InactivateSubscription();
		inactivateSubscription.setMsisdn(msisdn);
		inactivateSubscription.setSubscriptionId(subscriptionStatusId);
		payload.setInactivateSubscription(inactivateSubscription);
		vodacomRsaCancelRequestVO.setSubscriptionStatusId(subscriptionStatusId);
		vodacomRsaCancelRequestVO.setClientApplicationId("DCB_VIU");
		vodacomRsaCancelRequestVO.setId(100002);
		vodacomRsaCancelRequestVO.setLanguageLocale("en_ZA");
		vodacomRsaCancelRequestVO.setPurchaseLocale("en_ZA");
		vodacomRsaCancelRequestVO.setPayload(payload);
		return vodacomRsaCancelRequestVO;

	}
	

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	public String getPurchaseLocale() {
		return purchaseLocale;
	}

	public void setPurchaseLocale(String purchaseLocale) {
		this.purchaseLocale = purchaseLocale;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientApplicationId() {
		return clientApplicationId;
	}

	public void setClientApplicationId(String clientApplicationId) {
		this.clientApplicationId = clientApplicationId;
	}

	public String getLanguageLocale() {
		return languageLocale;
	}

	public void setLanguageLocale(String languageLocale) {
		this.languageLocale = languageLocale;
	}

	public String marshal() {
		String xml = "";
		try {
			ObjectMapper xmlMapper = VodacomUtill.getXmlObjectMapper();
			xml = xmlMapper.writeValueAsString(this);
		} catch (Exception ex) {
			LOGGER.error("VodacomRsaCancelRequestVO.marshal() - Exception occurred ", ex);
		}
		return xml;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "inactivateSubscription" })
	public static class Payload {

		// @XmlElement(name = "inactivate-subscription", required = true)
		@JacksonXmlProperty(localName = "inactivate-subscription")
		protected InactivateSubscription inactivateSubscription;

		public Payload() {
			this.inactivateSubscription = new InactivateSubscription();
		}

		public InactivateSubscription getInactivateSubscription() {
			return inactivateSubscription;
		}

		public void setInactivateSubscription(Payload.InactivateSubscription value) {
			this.inactivateSubscription = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "msisdn", "subscriptionId", })
		public static class InactivateSubscription {
			// @XmlElement(name = "msisdn")
			@JacksonXmlProperty(localName = "msisdn")
			protected String msisdn;
			// @XmlElement(name = "subscription-id")
			@JacksonXmlProperty(localName = "subscription-id")
			protected String subscriptionId;

			public String getMsisdn() {
				return msisdn;
			}

			public void setMsisdn(String value) {
				this.msisdn = value;
			}

			public String getSubscriptionId() {
				return subscriptionId;
			}

			public void setSubscriptionId(String value) {
				this.subscriptionId = value;
			}
		}

	}

}
