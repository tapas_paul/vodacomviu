package com.vuclip.vodacomviu.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.vuclip.vodacomviu.model.RequestResponseVO;
import com.vuclip.vodacomviu.model.VodacomNotificationRequestVO;
import com.vuclip.vodacomviu.utill.VodacomUtill;

@Controller
public class VodacomNotificationReceiver {

	private static final Logger LOGGER = LoggerFactory.getLogger(VodacomNotificationReceiver.class.getName());

	@RequestMapping(value = "/vodacomMockChargingNotificationReceiver", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String executeVSAVSNotificationReceiver(
			@RequestBody VodacomNotificationRequestVO vodacomRsaChargingNotificationRequestVO,
			HttpServletRequest httpServletRequest, Model model) {
		LOGGER.info("Inside Notification class");
		populateNotificationRequestVO(vodacomRsaChargingNotificationRequestVO, httpServletRequest);
		// Log Notification
		LOGGER.info("Notification from Vodacom, VodacomRsaChargingNotificationRequestVO = {}",
				vodacomRsaChargingNotificationRequestVO);
		// String packageSubscriptionId =
		// vodacomRsaChargingNotificationRequestVO.getPayload().getUsageAuthorisation().getPackageSubscriptionId();
		model.addAttribute("vodacomRsaChargingNotificationRequestVO", vodacomRsaChargingNotificationRequestVO);

		return "NotificationPage";
	}

	private void populateNotificationRequestVO(
			VodacomNotificationRequestVO vodacomRsaChargingNotificationRequestVO,
			HttpServletRequest httpServletRequest) {
		RequestResponseVO requestResponseVO = VodacomUtill.createRequestResponseVO(httpServletRequest);
		vodacomRsaChargingNotificationRequestVO.setRequestResponseVO(requestResponseVO);
	}

}
