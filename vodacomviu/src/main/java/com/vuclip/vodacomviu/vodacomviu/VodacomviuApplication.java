package com.vuclip.vodacomviu.vodacomviu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.vuclip.vodacomviu")
public class VodacomviuApplication {

	public static void main(String[] args) {
		SpringApplication.run(VodacomviuApplication.class, args);
	}
}
