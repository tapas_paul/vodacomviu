/**
 * 
 */
package com.vuclip.vodacomviu.utill;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.vuclip.vodacomviu.model.RequestResponseVO;

/**
 * @author Tapas Paul
 *
 */
@Component
public class VodacomUtill {
	
	private static final String DATE_FORMAT_IN_GMT = "dd-MM-yyyy HH:mm:ss 'GMT'";

	private static ObjectMapper xmlObjectMapper = new XmlMapper().registerModule(new JaxbAnnotationModule())
			.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	public static ObjectMapper getXmlObjectMapper() {
		return xmlObjectMapper;
	}

	public static RequestResponseVO createRequestResponseVO(HttpServletRequest httpServletRequest) {
		RequestResponseVO requestResponseVO = new RequestResponseVO();

		String notificationBody = getRawRequestBody(httpServletRequest);
		String notificationUrl = getNotificationRequestUrl(httpServletRequest);
		String notificationTimeStamp = VodacomUtill.getCurrentTimeStamp();

		requestResponseVO.setRequestBody(notificationBody);
		requestResponseVO.setRequestUrl(notificationUrl);
		requestResponseVO.setRequestTimeStamp(notificationTimeStamp);

		return requestResponseVO;
	}

	public static String getRawRequestBody(HttpServletRequest httpServletRequest) {
		String methodType = httpServletRequest.getMethod();
		String rawRequestBody = null;

		if ("GET".equals(methodType)) {
			rawRequestBody = getRequestBodyForGET(httpServletRequest);
		} else if ("POST".equals(methodType)) {
			rawRequestBody = getRequestBodyForPOST(httpServletRequest);
		}
		return removeLineBreaks(rawRequestBody);
	}

	public static String getNotificationRequestUrl(HttpServletRequest httpServletRequest) {
		return httpServletRequest.getRequestURI();
	}

	public static String getCurrentTimeStamp() {
		Date date = new Date();
		return formatDate(date, "GMT");
	}

	private static String getRequestBodyForGET(HttpServletRequest httpServletRequest) {
		return httpServletRequest.getQueryString();
	}

	private static String getRequestBodyForPOST(HttpServletRequest httpServletRequest) {
		ContentCachingRequestWrapper requestWrapper = WebUtils.getNativeRequest(httpServletRequest,
				ContentCachingRequestWrapper.class);
		String requestBody = "";

		if (requestWrapper != null) {
			byte[] buf = requestWrapper.getContentAsByteArray();
			if (buf.length > 0) {
				try {
					requestBody = new String(buf, requestWrapper.getCharacterEncoding());
				} catch (UnsupportedEncodingException ex) {
					requestBody = "[unknown]";
				}
			}
		}
		return requestBody;
	}

	public static String removeLineBreaks(String content) {
		if (content != null) {
			content = content.replaceAll("\r", "").replaceAll("\n", "");
		}
		return content;
	}

	public static String formatDate(Date date, String timeZone) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_IN_GMT);
		dateFormatter.setTimeZone(TimeZone.getTimeZone(timeZone));
		return dateFormatter.format(date);
	}
}
