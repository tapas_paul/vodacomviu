package com.vuclip.vodacomviu.model;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import com.vuclip.vodacomviu.model.VodacomSubscribeRequestVO.Payload.UsageAuthRateCharge;
import com.vuclip.vodacomviu.model.VodacomSubscribeRequestVO.Payload.UsageAuthRateCharge.ChargingId;
import com.vuclip.vodacomviu.model.VodacomSubscribeRequestVO.Payload.UsageAuthRateCharge.RatingAttributes;
import com.vuclip.vodacomviu.utill.VodacomUtill;

@Component("VodacomRequestVO")
@Scope("prototype")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "payload" })
@JacksonXmlRootElement(localName = "er-request")
public class VodacomSubscribeRequestVO {
	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	public String getPurchaseLocale() {
		return purchaseLocale;
	}

	public void setPurchaseLocale(String purchaseLocale) {
		this.purchaseLocale = purchaseLocale;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientApplicationId() {
		return clientApplicationId;
	}

	public void setClientApplicationId(String clientApplicationId) {
		this.clientApplicationId = clientApplicationId;
	}

	public String getLanguageLocale() {
		return languageLocale;
	}

	public void setLanguageLocale(String languageLocale) {
		this.languageLocale = languageLocale;
	}

	// LOGGER
	private static final Logger LOGGER = LoggerFactory.getLogger(VodacomSubscribeRequestVO.class.getName());

	@JacksonXmlProperty(localName = "payload")
	protected Payload payload;

	@JacksonXmlProperty(localName = "purchase_locale", isAttribute = true)
	protected String purchaseLocale;

	@JacksonXmlProperty(localName = "id", isAttribute = true)
	protected Integer id;

	@JacksonXmlProperty(localName = "client-application-id", isAttribute = true)
	protected String clientApplicationId;

	@JacksonXmlProperty(localName = "language_locale", isAttribute = true)
	protected String languageLocale;

	public VodacomSubscribeRequestVO() {
		payload = new Payload();
	}

	public VodacomSubscribeRequestVO populateRequestData(String msisdn, Integer channelId2) throws Exception {
		VodacomSubscribeRequestVO vodacomRequestVO = new VodacomSubscribeRequestVO();
		UsageAuthRateCharge usageAuthRateCharge = new UsageAuthRateCharge();
		
		Payload.UsageAuthRateCharge.ChargingId chargingId = usageAuthRateCharge.getChargingId();
		chargingId.setValue(msisdn);
		
		RatingAttributes ratingAttributes = new RatingAttributes();
		ratingAttributes.setAssetId("");
		if(null!=channelId2) {
			ratingAttributes.setChannel(channelId2);			
		}
		ratingAttributes.setContentName("vuclip_act_test");
		ratingAttributes.setPartnerId("DCB_VIU");

		usageAuthRateCharge.setChargingId(chargingId);
		usageAuthRateCharge.setRatingAttributes(ratingAttributes);
		usageAuthRateCharge.setServiceId("vc-viu-video-01");
		
		Payload payload = new Payload();
		payload.setUsageAuthRateCharge(usageAuthRateCharge);
		
		vodacomRequestVO.setClientApplicationId("DCB_VIU");
		vodacomRequestVO.setId(100017);
		vodacomRequestVO.setLanguageLocale("en_ZA");
		vodacomRequestVO.setPurchaseLocale("en_ZA");
		vodacomRequestVO.setPayload(payload);
		
		return vodacomRequestVO;
		
	}

	public String marshal() {
		String xml = "";
		try {
			ObjectMapper xmlMapper = VodacomUtill.getXmlObjectMapper();
			xml = xmlMapper.writeValueAsString(this);
		} catch (Exception ex) {
			LOGGER.error("VodacomRsaRegistrationRequestVO.marshal() - Exception occurred ", ex);
		}
		return xml;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "usageAuthRateCharge" })
	public static class Payload {

		@JacksonXmlProperty(localName = "usage-auth-rate")
		protected UsageAuthRateCharge usageAuthRateCharge;

		public Payload() {
			usageAuthRateCharge = new UsageAuthRateCharge();
		}

		public UsageAuthRateCharge getUsageAuthRateCharge() {
			return usageAuthRateCharge;
		}

		public void setUsageAuthRateCharge(UsageAuthRateCharge value) {
			this.usageAuthRateCharge = value;
		}

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "chargingId", "serviceId", "ratingAttributes"

		})
		public static class UsageAuthRateCharge {

			@JacksonXmlProperty(localName = "charging-id")
			protected ChargingId chargingId;

			@JacksonXmlProperty(localName = "service-id")
			protected String serviceId;

			@JacksonXmlProperty(localName = "rating-attributes")
			protected RatingAttributes ratingAttributes;

			public UsageAuthRateCharge() {
				ratingAttributes = new RatingAttributes();
				chargingId = new ChargingId();
			}

			public ChargingId getChargingId() {
				return chargingId;
			}

			public void setChargingId(ChargingId value) {
				this.chargingId = value;
			}

			public String getServiceId() {
				return serviceId;
			}

			public void setServiceId(String value) {
				this.serviceId = value;
			}

			public RatingAttributes getRatingAttributes() {
				return ratingAttributes;
			}

			public void setRatingAttributes(RatingAttributes value) {
				this.ratingAttributes = value;
			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "assetId", "contentName", "deviceType", "partnerId", "channel" })
			public static class RatingAttributes {

				@JacksonXmlProperty(localName = "asset-id")
				protected String assetId;

				@JacksonXmlProperty(localName = "content-name")
				protected String contentName;

				@JacksonXmlProperty(localName = "device-type")
				protected int deviceType;

				@JacksonXmlProperty(localName = "partner-id")
				protected String partnerId;

				@JacksonXmlProperty(localName = "channel")
				protected int channel;

				public int getChannel() {
					return channel;
				}

				public void setChannel(int channel) {
					this.channel = channel;
				}

				public String getAssetId() {
					return assetId;
				}

				public void setAssetId(String value) {
					this.assetId = value;
				}

				public String getContentName() {
					return contentName;
				}

				public void setContentName(String value) {
					this.contentName = value;
				}

				public int getDeviceType() {
					return deviceType;
				}

				public void setDeviceType(int value) {
					this.deviceType = value;
				}

				public String getPartnerId() {
					return partnerId;
				}

				public void setPartnerId(String value) {
					this.partnerId = value;
				}

			}

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "type", "value" })
			public class ChargingId {
				@JacksonXmlProperty(isAttribute = true)
				private String type = "msisdn";
				@JacksonXmlText
				private String value;

				public String getValue() {
					return value;
				}

				public void setValue(String value) {
					this.value = value;
				}
			}

		}

	}

}
