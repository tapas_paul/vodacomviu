/**
 * 
 */
package com.vuclip.vodacomviu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.vuclip.vodacomviu.model.VodacomSubscribeRequestVO;
import com.vuclip.vodacomviu.model.VodacomUnsubscribeRequestVO;



/**
 * @author tpaul
 *
 */
@Controller
public class VodacomUnsubscribeController {

	private static final Logger logger = LoggerFactory.getLogger(VodacomUnsubscribeController.class);
	
	@Value("${vodacom.uar.inactivesubcription.url}")
	private String url;
	
	@RequestMapping(value={"/msisdnUnsubsribe"}, method={RequestMethod.POST, RequestMethod.GET} )
	public String msisdnUnsubscribe(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("msisdn") String msisdn, @RequestParam("packageSubscriptionId") String packageSubscriptionId) {
		logger.info("request came to /msisdnUnsubscribe");
		logger.info("msisdn to unsubscribe: " + msisdn);
		logger.info("packageSubscriptionId to unsubscribe: " + packageSubscriptionId);
		
		RestTemplate restTemplate = new RestTemplate(
				new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic VnVjbGlwOkdlUTI3TWRwTmFyRg==");
		headers.add("Accept", "application/xml");
		HttpEntity<String> req = new HttpEntity<String>(
				new VodacomUnsubscribeRequestVO().populateRequestData(msisdn,packageSubscriptionId).marshal(), headers);

		ResponseEntity<String> resp = restTemplate.exchange(url, HttpMethod.POST, req, String.class);
		String deactivationResponse = resp.getBody();
		model.addAttribute("msisdn", msisdn);
		model.addAttribute("deactivationresponse", deactivationResponse);
		return "Deactivationresponse";
	}
	
}
